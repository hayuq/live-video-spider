package com.june.spider.model;

public class LiveVideoRoom {

	/**
	 * 直播间标题
	 */
	private String roomTitle;

	/**
	 * 直播间地址
	 */
	private String roomUrl;

	/**
	 * 封面图片地址
	 */
	private String imgUrl;

	/**
	 * 主播名
	 */
	private String anchorName;
	
	/**
	 * 直播间人气
	 */
	private String heats;

	public String getRoomTitle() {
		return roomTitle;
	}

	public void setRoomTitle(String roomTitle) {
		this.roomTitle = roomTitle;
	}

	public String getRoomUrl() {
		return roomUrl;
	}

	public void setRoomUrl(String roomUrl) {
		this.roomUrl = roomUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getAnchorName() {
		return anchorName;
	}

	public void setAnchorName(String anchorName) {
		this.anchorName = anchorName;
	}
	
	public String getHeats() {
		return heats;
	}
	
	public void setHeats(String heats) {
		this.heats = heats;
	}

	@Override
	public String toString() {
		return "LiveVideoRoom [roomTitle=" + roomTitle + ", roomUrl=" + roomUrl + ", imgUrl=" + imgUrl + ", anchorName="
				+ anchorName + ", heats=" + heats + "]";
	}

}
