package com.june.spider.util;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringBeanHolder implements ApplicationContextAware {

	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static <T> T getBean(Class<T> beanClass) {
		return applicationContext.getBean(beanClass);
	}

	public static <T> T getBean(String beanName, Class<T> beanClass) {
		return applicationContext.getBean(beanName, beanClass);
	}

	public static <T> Collection<T> getBeansByType(Class<T> type) {
		Map<String, T> beanMap = getBeanMapByType(type);
		return beanMap != null ? beanMap.values() : null;
	}
	
	public static <T> Map<String, T> getBeanMapByType(Class<T> type) {
		return applicationContext.getBeansOfType(type);
	}

}