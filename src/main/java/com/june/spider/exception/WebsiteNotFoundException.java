package com.june.spider.exception;

public class WebsiteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1464052282501782815L;
	
	public WebsiteNotFoundException() {
		super();
	}
	
	public WebsiteNotFoundException(String message) {
		super(message);
	}
	
	public WebsiteNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public WebsiteNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
