package com.june.spider.exception;

public class URLNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = -2065446562564317559L;

	public URLNotFoundException() {
		super();
	}
	
	public URLNotFoundException(String message) {
		super(message);
	}
	
	public URLNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public URLNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
