package com.june.spider.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.june.spider.service.SpiderService;

@Service
public class PandaTVSpiderServiceImpl implements SpiderService {
	
	@Value("${spider.panda.host}")
	private String host;
	
	@Value("${spider.panda.regex}")
	private String regex;
	
	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getHost() {
		return host;
	}
	
	@Override
	public String getHtml(String webSiteUrl) {
		// 通过添加User-Agent请求头模拟浏览器行为，绕过反爬虫检测
		Map<String, String> headers = new HashMap<>(1);
		headers.put(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 6.1; W…) Gecko/20100101 Firefox/59.0");
		return getHtml(webSiteUrl, headers);
	}
	
}
