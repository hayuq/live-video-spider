package com.june.spider.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.june.spider.service.SpiderService;

@Service
public class HuYaSpiderServiceImpl implements SpiderService {
	
	@Value("${spider.huya.host}")
	private String host;
	
	@Value("${spider.huya.regex}")
	private String regex;
	
	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getHost() {
		return host;
	}

}
